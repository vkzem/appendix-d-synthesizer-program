#ifndef SYNTH_H
#define SYNTH_H

#include <inttypes.h>
#include <stddef.h>

#include "audio.h"
#include "io.h"

#define AUDIO_TS (1.f/AUDIO_FS)

#define PI (3.1415927f)
#define TAU (2.f * PI)
#define INV_TAU (1.f/TAU)


float pow2_int(int x);
float pow2_frac(float x);
float pow2(float x);


// midi message receiver
struct midi_rx {
	struct synth *synth;	// pointer back to the parent synth state
	void (*func) (struct midi_rx * midi);	// event function
	int state;		// rx state
	uint8_t status;		// message status byte
	uint8_t arg0;		// message byte 0
	uint8_t arg1;		// message byte 1
};

void midi_rx_serial(struct midi_rx *midi, struct usart_drv *serial);
float midi_map(uint8_t val, float a, float b);
float midi_to_frequency(float note);


// event type in the upper 8 bits
#define EVENT_TYPE(x) ((x) & 0xff000000U)
#define EVENT_TYPE_KEY_DN (1U << 24)
#define EVENT_TYPE_KEY_UP (2U << 24)
#define EVENT_TYPE_MIDI (3U << 24)
#define EVENT_TYPE_AUDIO (4U << 24)

// key number in the lower 8 bits
#define EVENT_KEY(x) ((x) & 0xffU)
// midi message in the lower 3 bytes
#define EVENT_MIDI(x) ((x) & 0xffffffU)
// audio block size in the lower 16 bits
#define EVENT_BLOCK_SIZE(x) ((x) & 0xffffU)

struct event {
	uint32_t type;		// the event type
	void *ptr;		// pointer to event data (or data itself)
};

int event_init(void);
int event_rd(struct event *event);
int event_wr(uint32_t type, void *ptr);

// voices
#define VOICE_STATE_SIZE 1024*18

struct voice {
	int idx;		// index in table
	uint8_t note;		// current note
	uint8_t channel;	// current channel
	struct patch *patch;	// patch in use
	uint8_t state[VOICE_STATE_SIZE];	// per voice state
};

struct voice *voice_lookup(struct synth *s, uint8_t channel, uint8_t note);
struct voice *voice_alloc(struct synth *s, uint8_t channel, uint8_t note);
void update_voices(struct patch *p, void (*func) (struct voice *));

// patches
// patch operations
struct patch_ops {
	// voice functions
	void (*start) (struct voice * v);	// start a voice
	void (*note_on) (struct voice * v, uint8_t vel);
	void (*generate) (struct voice * v, float *out_l, float *out_r, size_t n);	// generate samples

	// patch functions
	void (*init) (struct patch * p);
	void (*control_change) (struct patch * p, uint8_t ctrl, uint8_t val);
};


#define PATCH_STATE_SIZE 128

struct patch {
	struct synth *synth;	// pointer back to the parent synth state
	const struct patch_ops *ops;
	uint8_t state[PATCH_STATE_SIZE];	// per patch state
};


extern const struct patch_ops patch;


// number of simultaneous voices
#define NUM_VOICES 6
// number of concurrent channels/patches
#define NUM_CHANNELS 1

struct synth {
	struct audio_drv *audio;	// audio output
	struct usart_drv *serial;	// serial port for midi interface
	struct display_drv *display; //display driver 
	struct midi_rx midi_rx0;	// midi rx from the serial port
	struct patch patches[NUM_CHANNELS];	// current patch set
	struct voice voices[NUM_VOICES];	// voices
	int voice_idx;		// round robin voice allocation
};

int synth_init(struct synth *s, struct audio_drv *audio, struct usart_drv *midi, struct display_drv *display);
int synth_run(struct synth *s);


#endif				// SYNTH_H

