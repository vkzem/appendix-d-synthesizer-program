#include <string.h>

#include "synth.h"

//-----------------------------------------------------------------------------

#define NUM_EVENTS 16

// circular buffer for events
struct event_queue {
	struct event queue[NUM_EVENTS];
	size_t rd;
	size_t wr;
};

static struct event_queue eq;

//-----------------------------------------------------------------------------

// read an event from the event queue
int event_rd(struct event *e) {
	struct event *x;
	uint32_t saved;
	int rc = 0;
	// mask interrupts
	saved = disable_irq();
	// do we have events?
	if (eq.rd == eq.wr) {
		// no events
		rc = -1;
		goto exit;
	}
	// copy the event data
	x = &eq.queue[eq.rd];
	e->type = x->type;
	e->ptr = x->ptr;
	// advance the read index
	eq.rd = (eq.rd + 1) & (NUM_EVENTS - 1);
 exit:
	// restore interrupts and return
	restore_irq(saved);
	return rc;
}

// write an event to the event queue
int event_wr(uint32_t type, void *ptr) {
	struct event *x;
	uint32_t saved;
	size_t wr;
	int rc = 0;
	// mask interrupts
	saved = disable_irq();
	wr = (eq.wr + 1) & (NUM_EVENTS - 1);
	if (wr == eq.rd) {
		// the queue is full
		rc = -1;
		goto exit;
	}
	// copy the event data
	x = &eq.queue[eq.wr];
	x->type = type;
	x->ptr = ptr;
	// advance the write index
	eq.wr = wr;
 exit:
	// restore interrupts and return
	restore_irq(saved);
	return rc;
}

//-----------------------------------------------------------------------------

// initialise event processing
int event_init(void) {
	memset(&eq, 0, sizeof(struct event_queue));
	return 0;
}

//-----------------------------------------------------------------------------
