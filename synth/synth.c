
//-----------------------------------------------------------------------------

#include <string.h>

#include "synth.h"
#define DEBUG
#include "logging.h"
#include "lcd.h"
#include "display.h"

//-----------------------------------------------------------------------------
// voice operations

// lookup the voice being used for this channel and note.
struct voice *voice_lookup(struct synth *s, uint8_t channel, uint8_t note) {
	for (unsigned int i = 0; i < NUM_VOICES; i++) {
		struct voice *v = &s->voices[i];
		if (v->note == note && v->channel == channel) {
			return v;
		}
	}
	return NULL;
}


// allocate a new voice, reusing a current active voice.
struct voice *voice_alloc(struct synth *s, uint8_t channel, uint8_t note) {
	// validate the channel
	if (channel >= NUM_CHANNELS || s->patches[channel].ops == NULL) {
		return NULL;
	}

	struct voice *v = &s->voices[s->voice_idx];
	s->voice_idx += 1;
	if (s->voice_idx == NUM_VOICES) {
		s->voice_idx = 0;
	}

	// setup the new voice
	v->note = note;
	v->channel = channel;
	v->patch = &s->patches[channel];
	v->patch->ops->start(v);
	return v;
}

// run an update function for each voice using the patch
void update_voices(struct patch *p, void (*func) (struct voice *)) {
	for (int i = 0; i < NUM_VOICES; i++) {
		struct voice *v = &p->synth->voices[i];
		if (v->patch == p) {
			func(v);
		}
	}
}


//-----------------------------------------------------------------------------
// audio request events

// add two buffers (2.31uS for n=128)
void block_add(float *out, float *buf, size_t n) {
	for (size_t i = 0; i < n; i++) {
		out[i] += buf[i];
	}
}


// handle an audio request event
static void audio_handler(struct synth *s, struct event *e) {
	size_t n = EVENT_BLOCK_SIZE(e->type);
	int16_t *dst = e->ptr;

	// clear the output buffers
	float out_l[n], out_r[n];
	memset(out_l, 0, n * sizeof(float));
	memset(out_r, 0, n * sizeof(float));

	for (int i = 0; i < NUM_VOICES; i++) {
		struct voice *v = &s->voices[i];
		struct patch *p = v->patch;

		if (p) {
			// generate left/right samples
			float buf_l[n], buf_r[n];
			p->ops->generate(v, buf_l, buf_r, n);
			// accumulate in the output buffers
			block_add(out_l, buf_l, n);
			block_add(out_r, buf_r, n);
		}
	}

	// write the samples to the dma buffer
	audio_wr(dst, n, out_l, out_r);
	// record some realtime stats
	audio_stats(s->audio, dst);
}

//-----------------------------------------------------------------------------

// synthesizer event loop
int synth_run(struct synth *s) {

	struct display_drv *ds = (struct display_drv *)s->display;

	print_one_line(&ds->term, "G2 C3 F4 G3 C4\n");
	
	while (1) {
		struct event e;
		
		if (!event_rd(&e)) {
		audio_handler(s, &e);
		}

		// get and process serial midi messages
		midi_rx_serial(&s->midi_rx0, s->serial);
	}
	return 0;
}

//-----------------------------------------------------------------------------

// initialise the synth state
int synth_init(struct synth *s, struct audio_drv *audio, struct usart_drv *serial, struct display_drv *display) {
	int rc = 0;

	memset(s, 0, sizeof(struct synth));
	s->audio = audio;
	s->serial = serial;
	s->display = display; //add in display driver to drive LCD!

	s->midi_rx0.synth = s;

	rc = event_init();

	s->patches[0].ops = &patch;
	struct patch *p = &s->patches[0];
	p->synth = s;
	memset(p->state, 0, PATCH_STATE_SIZE);
	p->ops->init(p);

	// setup the voices
	for (int i = 0; i < NUM_VOICES; i++) {
		struct voice *v = &s->voices[i];
		v->idx = i;
		v->channel = 255;
		v->note = 255;
	}

 exit:
	return rc;
}

//-----------------------------------------------------------------------------
