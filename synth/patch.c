//-----------------------------------------------------------------------------
/*

guitar synthesizer fyp

patch for string model created in faust

vishal khanna




*/
//-----------------------------------------------------------------------------

#include <assert.h>
#include <string.h>

#include <math.h>
#include "synth.h"


#include <stdio.h>
#include <stdlib.h>

#include "lcd.h"
#include "display.h"

#define DEBUG
#include "logging.h"


//max and min functions, function safe!
#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })


#define min(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b; })


//-----------------------------------------------------------------------------


//excitation waveform, pre calculated using matlab script
static float fmydspWave0[795] = {0.00168460002f,0.00151973998f,0.00165409001f,0.00151973998f,0.000982700032f,0.000909389986f,0.00104372995f,0.000634729979f,0.000585970003f,0.000784090022f,0.000107079999f,6.98899967e-05f,-0.000315770012f,-0.000331840012f,0.000653669995f,1.79100007e-05f,0.000781219976f,0.00043628001f,-6.7150002e-05f,6.71700036e-05f,-9.76899973e-05f,0.000433379988f,0.000665250001f,0.00113529002f,0.000845119997f,0.000415490009f,0.000513189996f,0.000152239998f,0.000460859999f,0.000662049977f,0.000686470012f,0.00031114f,-0.000191679996f,-0.000654359988f,-0.000124679995f,-0.000518820016f,-0.00012207f,3.37499987e-05f,0.000318000006f,0.000726170023f,0.00053095998f,0.000711079978f,-7.21000033e-05f,0.000155720001f,-0.000179690003f,0.000337250007f,0.000310680014f,-0.000304819987f,9.89e-06f,5.59e-06f,-1.22600004e-05f,-0.00046707f,-0.000219499998f,-0.000436509989f,7.02499965e-05f,0.00037845f,0.000314039993f,0.000159889998f,-0.000418970012f,-0.000455689995f,-0.00122392003f,-0.000564299989f,-0.000237920001f,-0.000378710014f,-0.00027135f,-0.000366540015f,8.99999975e-07f,-0.000593959994f,0.000150909997f,-9.19599988e-05f,7.93500003e-05f,-0.000704610022f,0.000577180006f,4.2489999e-05f,-0.000796369975f,-0.000433289999f,-0.00108343002f,-0.000945999986f,-0.00053701998f,-0.000732389977f,-4.87299985e-05f,5.78299987e-05f,-0.000683450024f,-0.000555420003f,-0.000802299997f,-0.000891230011f,-0.000393669994f,0.000137020004f,-0.000185490004f,-0.000392949994f,-0.000613750017f,-0.00145377999f,-0.00114694994f,-0.00143707998f,-0.000755889982f,9.2500004e-06f,6.02999989e-06f,-0.00055502f,-0.000558029977f,-0.00101315998f,-0.00101916003f,-0.000433350011f,-0.00129058002f,-0.00117516995f,-0.00161862001f,-0.00191368f,-0.00182147999f,-0.00115059002f,-0.00138531998f,-0.00103786995f,-0.00119682995f,-0.00152195001f,-0.00117425004f,-0.00127879996f,-0.00131167995f,-0.000929609989f,-0.00156936003f,-0.00194999995f,-0.000768340018f,-0.00152610999f,-0.000821090012f,-0.000964239996f,-0.00134274003f,-0.00126944005f,-0.00125443004f,-0.00137888f,-0.000932539988f,-0.000937570003f,-0.00126076001f,-0.000564310001f,-0.00153508002f,-0.000830059987f,-0.000412480003f,-0.00131451001f,-0.00124469004f,-0.00130609004f,-0.00127557002f,-0.00127557002f,-0.00127557002f,-0.00124506f,-0.00137036003f,-0.00200183992f,-0.00186154002f,-0.00139516999f,-0.00117885997f,-0.000779010006f,-0.00137196004f,-0.000774569984f,-0.000604250003f,-0.00067429f,6.10099996e-05f,-8.83199973e-05f,0.000525149982f,0.000487870013f,-0.000140389995f,9.75000003e-05f,-0.000161160002f,-0.000487429992f,5.31900005e-05f,-0.000240370005f,-0.000711200002f,-0.000303790002f,5.79400003e-05f,-0.000116069998f,0.000689510023f,0.00086356001f,0.000558770029f,0.000714009977f,0.000528139994f,0.000796150009f,-0.000203100004f,-0.000423410005f,9.52900009e-05f,0.000594789977f,0.000650070026f,0.00064695999f,0.000497129979f,-0.000198130001f,-0.000420560013f,-0.000168269995f,-0.000639540027f,-0.000652500021f,-0.00066528999f,-0.000616490026f,-0.000665250001f,-0.000643769978f,-0.000640889979f,-0.000640870014f,-0.000122500001f,-0.000558089989f,1.72199998e-05f,-0.000161499993f,-0.000620610022f,-0.00116843998f,-0.000787339988f,-0.000552219979f,-0.000732439978f,-0.000518799992f,-0.000823970011f,-0.000244139999f,-0.000335690012f,-0.000393489987f,0.00014643f,-0.000252350001f,-0.000638530008f,-0.000594749989f,-0.000677349977f,-0.000264689996f,6.70700028e-05f,-5.20799986e-05f,0.000542770023f,0.000631330011f,0.000634679978f,0.000634700002f,0.000634700002f,0.000634700002f,0.000665220025f,0.000604189991f,0.000665220025f,0.000576900027f,0.000747380021f,-0.000118750002f,-0.000183380005f,-6.26000019e-06f,1.49400003e-05f,-0.000164040001f,-0.000623870001f,-0.000864979986f,-0.000760330004f,-0.000539890025f,-0.000732249988f,-0.000158459996f,5.46800002e-05f,-0.000216900007f,-0.000756779977f,-0.000558689993f,-0.000801979972f,-0.00131020998f,-0.000710639986f,-0.000622450025f,-0.00128784997f,-0.00131786999f,-0.000860399974f,-0.000894170022f,-0.000256539992f,-0.000863620022f,-0.000786620018f,0.000105589999f,-9.49500027e-05f,0.000124829996f,-0.00036743001f,-0.000371419999f,-0.000564149988f,-0.000683980004f,-0.000592069991f,-0.000689629989f,-0.000622619991f,-0.000659119978f,-0.000683659979f,-0.00022863f,-0.00029632001f,-0.00107870996f,-0.00107213994f,-0.00180085003f,-0.00135449995f,-0.000698600023f,-0.00138258003f,-0.00122355996f,-0.00127269002f,-0.00127554999f,-0.00122101f,-0.00151851005f,-0.00124390004f,-0.000405230006f,-0.00180466997f,-0.00142741995f,-0.00142492994f,-0.00193479005f,-0.00198650989f,-0.00154734997f,-0.000895429985f,-0.00147731998f,-0.00109300006f,-0.00165562006f,-0.00164083997f,-0.00121082005f,-0.000653500028f,-0.000649870024f,-0.000634750002f,-0.000186439996f,-0.000466560014f,-0.00103106f,-0.00131839002f,-0.00127560005f,-0.00125735998f,-0.0013912f,-0.000352739997f,-0.000680279976f,-0.000488789985f,-0.000598469982f,-0.00244426006f,-0.00224613003f,-0.000622459978f,-0.00148649001f,-0.000979279983f,0.00119788002f,-0.000763570017f,-0.000539590023f,0.00210007001f,0.00134456996f,0.000696250005f,0.0017577f,0.000939699996f,-0.000249580014f,-0.000563559995f,-0.000248900003f,-0.00218752003f,-0.00256993994f,-0.00232341001f,-0.000938970014f,0.00145326997f,0.00100387004f,0.00121171004f,0.0046265698f,-0.0041565001f,-0.0111204796f,-0.00720825978f,-0.0119444504f,-0.0140811801f,-0.0128860604f,-0.0120059196f,-0.0109491199f,-0.00981437042f,-0.0081936596f,-0.00942375977f,-0.0101443203f,-0.0113042202f,-0.00954824965f,-0.0102920597f,-0.0108260503f,-0.00853640959f,-0.00725963013f,-0.00518452004f,-0.00324632996f,-0.00395527994f,-0.0033019199f,0.000460899988f,-0.00326852011f,-0.00394375017f,-0.00201773993f,-0.00329857995f,-0.00547026005f,-0.00612846995f,-0.0032160501f,-0.00163802004f,0.000100819998f,0.00400059996f,0.00537435012f,0.0109838396f,0.00941140018f,0.000162960001f,-0.00105922006f,-0.0046011298f,-0.00721450988f,-0.00664395979f,-0.00592130003f,-0.00305807008f,-0.00180028996f,0.00146804994f,0.00614282023f,0.0101491502f,0.00612137001f,0.00141608005f,-0.00230108993f,-0.00703082001f,-0.0100634601f,-0.00720490981f,-0.00443169009f,-0.00412914017f,-0.000103899998f,0.00507220021f,0.00953998044f,0.00911253039f,0.00541064981f,0.00115354999f,0.000524930016f,-0.000433379988f,-0.00270993006f,0.00191647001f,0.00284104003f,0.00183107005f,0.00936846994f,0.0165858902f,0.0217895005f,0.0252867695f,0.0229154397f,0.0171142202f,0.0106412303f,0.0110796997f,0.00779407006f,0.00333574996f,0.00432136981f,0.0110257203f,0.0156800803f,0.0182795301f,0.0247385204f,0.0195682608f,0.0133024501f,0.00755015016f,0.00380210998f,-0.000913140015f,-0.00255081989f,-0.00269477f,-0.00177286996f,0.00327443006f,0.00512947002f,0.00593366008f,0.00849603955f,0.00656215008f,0.00365912006f,0.00103882002f,-0.00149015f,-0.00526469992f,-0.00621946994f,-0.00651548989f,-0.00166886998f,0.00588918012f,0.00561803998f,0.0098750703f,0.0165872294f,0.0165199097f,0.0156946499f,0.0165621992f,0.0136201903f,0.0128851105f,0.0126919597f,0.0150846299f,0.0196043905f,0.0221072994f,0.0233350396f,0.0220372807f,0.0224820692f,0.0197026003f,0.0136759803f,0.0065970202f,0.00291723991f,0.00123641f,0.00233732001f,0.00658559008f,0.00722650019f,0.00683947979f,0.0056474302f,0.00712300977f,0.00731858006f,0.00634885998f,0.00214467011f,-0.00188016996f,-0.00596887991f,-0.00560300006f,-0.000405769999f,0.000545300019f,0.00448920997f,0.00508840987f,0.00686455984f,0.01171978f,0.0129891699f,0.0125249596f,0.0105646905f,0.00932611991f,0.00640542991f,0.0114379497f,0.0123229399f,0.0103795202f,0.0130535001f,0.0141309798f,0.0168711003f,0.0203500502f,0.0213111192f,0.0143488497f,0.00725091994f,-0.000247399992f,-0.00467533013f,-0.00186773995f,-0.00235277996f,-0.00191412005f,-0.00363946008f,-0.00473383022f,-0.0093535902f,-0.0124940397f,-0.0156068103f,-0.0209843796f,-0.0250706896f,-0.0257414505f,-0.0212858897f,-0.0240377598f,-0.0246700607f,-0.0224454291f,-0.0206612498f,-0.0176931806f,-0.0152561702f,-0.0152983498f,-0.0172791295f,-0.0163025502f,-0.0146851204f,-0.0112333996f,-0.00864879973f,-0.00872805994f,-0.00798061956f,-0.00513840001f,-0.00284517999f,-0.00263620005f,-0.00399171002f,-0.00525515992f,-0.0048124101f,-0.00781552028f,-0.00813630968f,-0.00939949043f,-0.0161732491f,-0.0181026701f,-0.0143644102f,-0.0129984403f,-0.0173301809f,-0.0211704206f,-0.0267485809f,-0.0324402303f,-0.03434458f,-0.0313753299f,-0.0313782208f,-0.034918271f,-0.0329041108f,-0.0332398117f,-0.032995671f,-0.0311340895f,-0.0322632492f,-0.0357727706f,-0.0347046517f,-0.0322600082f,-0.02945574f,-0.0238618106f,-0.0217129309f,-0.0168397296f,-0.0117948297f,-0.00919247046f,-0.0101395398f,-0.01142524f,-0.0135868499f,-0.0123419203f,-0.0104581499f,-0.008072f,-0.00275560003f,-0.00374755007f,-0.00626224f,-0.00621944992f,-0.00583499018f,-0.0054902602f,-0.00558470003f,-0.0110473596f,-0.0159606896f,-0.0167236291f,-0.0201415997f,-0.0214506201f,-0.0179504901f,-0.0189452805f,-0.0195068698f,-0.0165921897f,-0.0153177697f,-0.01797105f,-0.0205938201f,-0.0238159709f,-0.0259432103f,-0.0237793401f,-0.0201204792f,-0.0160823409f,-0.0124783199f,-0.0100707803f,-0.00738848979f,-0.0020047701f,0.00207400997f,0.00034241f,-0.000757370028f,0.000842229987f,-0.000461029995f,-1.21800003e-05f,0.00694607012f,0.00992960017f,0.00882526021f,0.0105068898f,0.0117471898f,0.0098826997f,0.00825267006f,0.00625429023f,0.00515120989f,0.00298795011f,0.00189207005f,0.00167847006f,-0.000155820002f,-0.00159088999f,-0.00214281003f,-0.00235602004f,-0.00327154994f,-0.00480066007f,-0.0085754497f,-0.00911301002f,-0.00830748025f,-0.00744273001f,-0.00626838021f,-0.00327760004f,-0.000580030028f,-0.000219289999f,0.00387659995f,0.00851302966f,0.01150769f,0.0113434903f,0.0117430799f,0.01171258f,0.00890173018f,0.0104582496f,0.0131716803f,0.0152091403f,0.0182584208f,0.0238609593f,0.0260696504f,0.0246329401f,0.0219163299f,0.0154639101f,0.0140080396f,0.0145014301f,0.0156004401f,0.0165067203f,0.01646428f,0.0154702002f,0.0157351792f,0.0169372298f,0.0147549696f,0.0155428099f,0.0149914604f,0.0123750698f,0.0102504697f,0.00819402002f,0.00697294017f,0.00734899007f,0.00999359973f,0.0117585501f,0.0172945801f,0.0212811492f,0.0224739797f,0.0219243895f,0.0239337608f,0.0226446893f,0.0205624104f,0.02180258f,0.0215996206f,0.0213037394f,0.0237038508f,0.0303979293f,0.0375922397f,0.0427954309f,0.0462876707f,0.0442338586f,0.0374636613f,0.0338640399f,0.0328302607f,0.0300807599f,0.0256185196f,0.0231577903f,0.0243108496f,0.0273391791f,0.0259326193f,0.0339936316f,0.0375619009f,0.0305727609f,0.0314600393f,0.0304992199f,0.0286312904f,0.0268740803f,0.0240251292f,0.0229765605f,0.0264909603f,0.0310794096f,0.032173451f,0.0349197388f,0.0319053307f,0.0292393994f,0.02997352f,0.0276698992f,0.0236241892f,0.0204759594f,0.0193487797f,0.0214743596f,0.0255421009f,0.0278934296f,0.0343715511f,0.0363902301f,0.0335757211f,0.0321277007f,0.0304064695f,0.0270076096f,0.0252605509f,0.0233182907f,0.0236748196f,0.0232431199f,0.0224372093f,0.0165080391f,0.0155295003f,0.0206221603f,0.0200864207f,0.0230006799f,0.0223009009f,0.0200827401f,0.0171546191f,0.0128781796f,0.0121563999f,0.00772724021f,0.00241249008f,-0.00233133999f,-0.00192905997f,-0.000890630006f,0.00281093991f,0.00620341999f,0.00664154999f,0.00645590015f,0.00513073988f,0.00440096017f,0.00120832003f,-0.00201475993f,-0.00403170008f,-0.00295038009f,0.000678459997f,0.00542253023f,0.00815572031f,0.00755803008f,0.00781540014f,0.00421157992f,0.00326486002f,0.00431573018f,-0.000307109993f,-0.00758248009f,-0.0151546802f,-0.0194161609f,-0.0185340196f,-0.0155081199f,-0.0109409401f,-0.0124312798f,-0.0114612598f,-0.00819666032f,-0.00937348045f,-0.0196809508f,-0.0272081699f,-0.0301439296f,-0.038128119f,-0.0338035002f,-0.0256586391f,-0.0187858902f,-0.0148039898f,-0.00831437018f,-0.00761125004f,-0.00859898981f,-0.00816856977f,-0.0161666796f,-0.0189544298f,-0.0202501006f,-0.0203099698f,-0.0148254801f,-0.0098095797f,-0.00343872001f,-0.00209197006f,-0.000649820024f,-0.00218602992f,-0.000849929987f,-0.00408035982f,-0.0113508003f,-0.00961393956f,-0.0161074605f,-0.0238868892f,-0.0251742397f,-0.0239681508f,-0.0254880693f,-0.0241007302f,-0.0237697605f,-0.0252010599f,-0.0295574293f,-0.0348817408f,-0.0382603593f,-0.0434937589f,-0.0445514582f,-0.0446105786f,-0.0423290506f,-0.0348630883f,-0.0260448195f,-0.0231451597f,-0.0217001494f,-0.0209481101f,-0.0246797297f,-0.0249879304f,-0.0261220504f};


//holds all state information of a partcicular voice
struct voice_state {
	
	float vol;		

	float fVec0[2];
	float fRec1[2];
	float fButton0;
	int fmydspWave0_idx;
	int voice_idx;
	float fVec1[512];

	float fVec2[1024];
	float fRec0[2];
	float fVec3[2];
	float fRec3[2];
	float fVec4[1024];
	float fRec2[2];
	float fVec5[2];

	float fVec6[2];
	float fRec5[2];

	float fConst0;
	float fRec7[2];
	float fRec6[3];
	float fVec7[256];
	float fRec4[2];
	float fVec8[2];
	float fRec9[2];
	float fVec9[256];
	float fRec8[2];
	float fVec10[2];
	float fRec11[2];
	float fVec11[256];
	float fRec10[2];
	float fVec12[2];
	float fRec13[2];
	float fVec13[512];
	float fRec12[2];
	float fVec14[2];
	float fRec15[2];
	float fVec15[512];
	float fRec14[2];
};


struct patch_state {
	//volume setting
	float vol;		

	//string model settings
	float pol_mix;
	float detune;
	float feedback;
	float pluck;
	float coupling;
	float symp_feedback;
	float bridge_cutoff;
	
	//sympathetic string midi note numbers
	int symp_notes1;
	int symp_notes2;
	int symp_notes3;
	int symp_notes4;
	int symp_notes5;

};


//clear the voice memory, function comes from faust
void reset_voice_state(struct voice_state* vs) {
		
		int l0;
		for (l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			vs->fVec0[l0] = 0.0f;
		}
		int l1;
		for (l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			vs->fRec1[l1] = 0.0f;
		}
		vs->voice_idx = 0;
		int l2;
		for (l2 = 0; (l2 < 512); l2 = (l2 + 1)) {
			vs->fVec1[l2] = 0.0f;
		}
		int l3;
		for (l3 = 0; (l3 < 1024); l3 = (l3 + 1)) {
			vs->fVec2[l3] = 0.0f;
		}
		int l4;
		for (l4 = 0; (l4 < 2); l4 = (l4 + 1)) {
			vs->fRec0[l4] = 0.0f;
		}
		int l5;
		for (l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			vs->fVec3[l5] = 0.0f;
		}
		int l6;
		for (l6 = 0; (l6 < 2); l6 = (l6 + 1)) {
			vs->fRec3[l6] = 0.0f;
		}
		int l7;
		for (l7 = 0; (l7 < 1024); l7 = (l7 + 1)) {
			vs->fVec4[l7] = 0.0f;
		}
		int l8;
		for (l8 = 0; (l8 < 2); l8 = (l8 + 1)) {
			vs->fRec2[l8] = 0.0f;
		}
		int l9;
		for (l9 = 0; (l9 < 2); l9 = (l9 + 1)) {
			vs->fVec5[l9] = 0.0f;
		}
		int l10;
		for (l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			vs->fVec6[l10] = 0.0f;
		}
		int l11;
		for (l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			vs->fRec5[l11] = 0.0f;
		}
		int l12;
		for (l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			vs->fRec7[l12] = 0.0f;
		}
		int l13;
		for (l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			vs->fRec6[l13] = 0.0f;
		}
		int l14;
		for (l14 = 0; (l14 < 256); l14 = (l14 + 1)) {
			vs->fVec7[l14] = 0.0f;
		}
		int l15;
		for (l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			vs->fRec4[l15] = 0.0f;
		}
		int l16;
		for (l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			vs->fVec8[l16] = 0.0f;
		}
		int l17;
		for (l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			vs->fRec9[l17] = 0.0f;
		}
		int l18;
		for (l18 = 0; (l18 < 256); l18 = (l18 + 1)) {
			vs->fVec9[l18] = 0.0f;
		}
		int l19;
		for (l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			vs->fRec8[l19] = 0.0f;
		}
		int l20;
		for (l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			vs->fVec10[l20] = 0.0f;
		}
		int l21;
		for (l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			vs->fRec11[l21] = 0.0f;
		}
		int l22;
		for (l22 = 0; (l22 < 256); l22 = (l22 + 1)) {
			vs->fVec11[l22] = 0.0f;
		}
		int l23;
		for (l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			vs->fRec10[l23] = 0.0f;
		}
		int l24;
		for (l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			vs->fVec12[l24] = 0.0f;
		}
		int l25;
		for (l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			vs->fRec13[l25] = 0.0f;
		}
		int l26;
		for (l26 = 0; (l26 < 512); l26 = (l26 + 1)) {
			vs->fVec13[l26] = 0.0f;
		}
		int l27;
		for (l27 = 0; (l27 < 2); l27 = (l27 + 1)) {
			vs->fRec12[l27] = 0.0f;
		}
		int l28;
		for (l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			vs->fVec14[l28] = 0.0f;
		}
		int l29;
		for (l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			vs->fRec15[l29] = 0.0f;
		}
		int l30;
		for (l30 = 0; (l30 < 512); l30 = (l30 + 1)) {
			vs->fVec15[l30] = 0.0f;
		}
		int l31;
		for (l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			vs->fRec14[l31] = 0.0f;
		}
}



//AUDIO GENERATION FUNCTION
//run the model for [count] samples
//input pointer tot he voice state
//outputs the into the outputs[] buffer

//note that the code was initially generated by faust compiler
//however it has been heavily edited to interface with midi events, draw from the patch and voice states
//and add other changes where necessary.
//the important variables to note have been renamed for ease of understanding
void run_model(struct voice* v, int count, float* outputs) {
	struct voice_state *vs = (struct voice_state *)v->state;
	struct patch_state *ps = (struct patch_state *)v->patch->state;


	//parameters to match line to note duration control
	//calculated in excel
	float M = 8.22361937257211E-05;
	float C = 0.993421104501942;

	//polarization mixing coefficient from patch state
	float polarization_mix = ps->pol_mix;

	//delay line lengths for the string, one for each polarization
	float string_length_x = 44100.0/midi_to_frequency((float)v->note);
	float string_length_y = (ps->detune + string_length_x);

	//integer and decimal parts of string length
	float fSlow3 = (int)(string_length_y);
	float fSlow4 = (string_length_y - fSlow3);

	float fSlow5 = ps->feedback*((float)v->note*M+C);

	float fSlow6 = (0.891300023f * fSlow5);

	//gate for excitation waveform
	int waveform_gate = 1;


	int iSlow8 = (int)min(256.0f, max(0.0f, (string_length_x * ps->pluck)));

	int iSlow9 = (int)string_length_y;
	int iSlow10 = (int)min(513.0f, (float)max(0, (iSlow9 + 1)));
	float fSlow11 = (fSlow3 + (1.0f - string_length_y));
	int iSlow12 = (int)min(513.0f, (float)max(0, iSlow9));


	//1 minus pol mix for y plane wave
	float inv_pol_mix = (1.0f - polarization_mix);


	//faust parameters
	float fSlow14 = (int)(string_length_x);
	float fSlow15 = (string_length_x - fSlow14);
	int iSlow16 = fSlow14;
	int iSlow17 = (int)min(513.0f, (float)max(0, (iSlow16 + 1)));
	float fSlow18 = (fSlow14 + (1.0f - string_length_x));
	int iSlow19 = (int)min(513.0f, (float)max(0, iSlow16));
	float fSlow20 = ps->coupling;
	float fSlow21 = ps->symp_feedback;
	float fSlow22 = (0.891300023f * fSlow21);
	float fSlow23 = tanf((vs->fConst0 * ps->bridge_cutoff));
	float fSlow24 = (1.0f / fSlow23);
	float fSlow25 = (1.0f / (((fSlow24 + 1.0f) / fSlow23) + 1.0f));
	float fSlow26 = (fSlow24 + 1.0f);
	float fSlow27 = (0.0f - ((1.0f - fSlow24) / fSlow26));
	float fSlow28 = (1.0f / fSlow26);
	float fSlow29 = (((fSlow24 + -1.0f) / fSlow23) + 1.0f);
	float fSlow30 = (2.0f * (1.0f - (1.0f /(fSlow23*fSlow23))));

	//sympathetic string note delay lengths from patch state settings
	int n1 = 44100.0/midi_to_frequency(ps->symp_notes1);
	int n2 = 44100.0/midi_to_frequency(ps->symp_notes2);
	int n3 = 44100.0/midi_to_frequency(ps->symp_notes3);
	int n4 = 44100.0/midi_to_frequency(ps->symp_notes4);
	int n5 = 44100.0/midi_to_frequency(ps->symp_notes5);

	//
	int i;
	for (i = 0; (i < count); i = (i + 1)) {

		//close the waveform gate when we've reached the end for a particular voice
		if (vs->voice_idx > 795)
			waveform_gate = 0;

		vs->fVec0[0] = (fSlow5 * vs->fRec0[1]);

		//feedback filter
		vs->fRec1[0] = ((0.0257300008f * vs->fRec1[1]) + ((0.0723100007f * vs->fVec0[1]) + (fSlow6 * vs->fRec0[1])));
		
		//inject wave, after its finished inject 0
		float fTemp0 = (waveform_gate?fmydspWave0[vs->fmydspWave0_idx]:0.0f);


		vs->fVec1[(vs->voice_idx & 511)] = fTemp0;
		float fTemp1 = ((vs->fRec1[0] + vs->fVec1[((vs->voice_idx - iSlow8) & 511)]) - fTemp0);
		vs->fVec2[(vs->voice_idx & 1023)] = fTemp1;


		vs->fRec0[0] = ((fSlow4 * vs->fVec2[((vs->voice_idx - iSlow10) & 1023)]) + (fSlow11 * vs->fVec2[((vs->voice_idx - iSlow12) & 1023)]));
		vs->fVec3[0] = (fSlow5 * vs->fRec2[1]);


		//two SDL loop handling
		vs->fRec3[0] = ((0.0257300008f * vs->fRec3[1]) + ((0.0723100007f * vs->fVec3[1]) + (fSlow6 * vs->fRec2[1])));
		float fTemp2 = ((vs->fRec3[0] + vs->fVec1[((vs->voice_idx - iSlow8) & 511)]) - fTemp0);
		vs->fVec4[(vs->voice_idx & 1023)] = fTemp2;
		vs->fRec2[0] = ((fSlow15 * vs->fVec4[((vs->voice_idx - iSlow17) & 1023)]) + (fSlow18 * vs->fVec4[((vs->voice_idx - iSlow19) & 1023)]));
		float fTemp3 = ((polarization_mix * vs->fRec0[0]) + (inv_pol_mix * vs->fRec2[0]));
		vs->fVec5[0] = fTemp3;
		vs->fVec6[0] = (fSlow21 * vs->fRec4[1]);
		vs->fRec5[0] = ((0.0257300008f * vs->fRec5[1]) + ((0.0723100007f * vs->fVec6[1]) + (fSlow22 * vs->fRec4[1])));
		vs->fRec7[0] = ((fSlow27 * vs->fRec7[1]) + (fSlow28 * (fTemp3 + vs->fVec5[1])));
		vs->fRec6[0] = (vs->fRec7[0] - (fSlow25 * ((fSlow29 * vs->fRec6[2]) + (fSlow30 * vs->fRec6[1]))));
		float fTemp4 = (fSlow25 * (vs->fRec6[0] + (vs->fRec6[2] + (2.0f * vs->fRec6[1]))));
		vs->fVec7[(vs->voice_idx & 255)] = (vs->fRec5[0] + fTemp4);
		
		
		//sympathetic string handling, notes arent in any particular order

		//note 4
		vs->fRec4[0] = vs->fVec7[((vs->voice_idx - n4) & 255)];
		vs->fVec8[0] = (fSlow21 * vs->fRec8[1]);
		vs->fRec9[0] = ((0.0257300008f * vs->fRec9[1]) + ((0.0723100007f * vs->fVec8[1]) + (fSlow22 * vs->fRec8[1])));
		vs->fVec9[(vs->voice_idx & 255)] = (vs->fRec9[0] + fTemp4);
		//note 5
		vs->fRec8[0] = vs->fVec9[((vs->voice_idx - n5) & 255)];
		vs->fVec10[0] = (fSlow21 * vs->fRec10[1]);
		vs->fRec11[0] = ((0.0257300008f * vs->fRec11[1]) + ((0.0723100007f * vs->fVec10[1]) + (fSlow22 * vs->fRec10[1])));
		vs->fVec11[(vs->voice_idx & 255)] = (vs->fRec11[0] + fTemp4);
		//note 3
		vs->fRec10[0] = vs->fVec11[((vs->voice_idx - n3) & 255)];
		vs->fVec12[0] = (fSlow21 * vs->fRec12[1]);
		vs->fRec13[0] = ((0.0257300008f * vs->fRec13[1]) + ((0.0723100007f * vs->fVec12[1]) + (fSlow22 * vs->fRec12[1])));
		vs->fVec13[(vs->voice_idx & 511)] = (vs->fRec13[0] + fTemp4);
		//note 1
		vs->fRec12[0] = vs->fVec13[((vs->voice_idx - n1) & 511)];
		vs->fVec14[0] = (fSlow21 * vs->fRec14[1]);
		vs->fRec15[0] = ((0.0257300008f * vs->fRec15[1]) + ((0.0723100007f * vs->fVec14[1]) + (fSlow22 * vs->fRec14[1])));
		vs->fVec15[(vs->voice_idx & 511)] = (vs->fRec15[0] + fTemp4);
		//note 2
		vs->fRec14[0] = vs->fVec15[((vs->voice_idx - n2) & 511)];
		
		//write to output vector
		outputs[i] = (float)(fTemp3 + (fSlow20 * (vs->fRec4[0] + (vs->fRec8[0] + (vs->fRec10[0] + (vs->fRec12[0] + vs->fRec14[0]))))));
		
	

		//advance voice counter and excitation waveform index
		vs->fmydspWave0_idx = ((1 + vs->fmydspWave0_idx) % 795);
		vs->voice_idx = (vs->voice_idx + 1);


		//update stored vals for filters, delays etc (faust generated)
		vs->fVec0[1] = vs->fVec0[0];
		vs->fRec1[1] = vs->fRec1[0];
		vs->fRec0[1] = vs->fRec0[0];
		vs->fVec3[1] = vs->fVec3[0];
		vs->fRec3[1] = vs->fRec3[0];
		vs->fRec2[1] = vs->fRec2[0];
		vs->fVec5[1] = vs->fVec5[0];
		vs->fVec6[1] = vs->fVec6[0];
		vs->fRec5[1] = vs->fRec5[0];
		vs->fRec7[1] = vs->fRec7[0];
		vs->fRec6[2] = vs->fRec6[1];
		vs->fRec6[1] = vs->fRec6[0];
		vs->fRec4[1] = vs->fRec4[0];
		vs->fVec8[1] = vs->fVec8[0];
		vs->fRec9[1] = vs->fRec9[0];
		vs->fRec8[1] = vs->fRec8[0];
		vs->fVec10[1] = vs->fVec10[0];
		vs->fRec11[1] = vs->fRec11[0];
		vs->fRec10[1] = vs->fRec10[0];
		vs->fVec12[1] = vs->fVec12[0];
		vs->fRec13[1] = vs->fRec13[0];
		vs->fRec12[1] = vs->fRec12[0];
		vs->fVec14[1] = vs->fVec14[0];
		vs->fRec15[1] = vs->fRec15[0];
		vs->fRec14[1] = vs->fRec14[0];
		
		
		
	}
	
}

// start the voice (allocated)
static void start(struct voice *v) {
	struct voice_state *vs = (struct voice_state *)v->state;
	struct patch_state *ps = (struct patch_state *)v->patch->state;

	vs->vol = 1;

	//clear the entire voice state
    reset_voice_state(vs);
	
	//init the excitation waveform index
	vs->fmydspWave0_idx = 0;

	vs->fConst0 = (3.14159274f / 44100);

	
}

// note on
// resets the voice index, this will then cause the excitation waveform to be injected
static void note_on(struct voice *v, uint8_t vel) {
	struct voice_state *vs = (struct voice_state *)v->state;

	//clear voice state before we reinject energy, else overaccumulation/distortion occurs on successive keypresses
    reset_voice_state(vs);
    vs->voice_idx = 0;

}

// generate samples
static void generate(struct voice *v, float *out_l, float *out_r, size_t n) {
	struct voice_state *vs = (struct voice_state *)v->state;
	struct patch_state *ps = (struct patch_state *)v->patch->state;

	float out[n];
	int i=0;
    run_model(v, n, out);
		
	while(i<128){
		out_l[i] = 3*ps->vol*out[i];
		out_r[i] = 3*ps->vol*out[i];
		i++;
	} //vol ctrl

}

//-----------------------------------------------------------------------------
// global operations
static void init(struct patch *p) {
	struct patch_state *ps = (struct patch_state *)p->state;
	ps->vol = 1.5f;

	ps->pol_mix = 0.33f;
	ps->detune = 0.2f;
	ps->feedback = 1.005f;
	ps->pluck = 0.3f;
	ps->coupling = 0.03f;
	ps->symp_feedback = 1.f;
	ps->bridge_cutoff = 1000.f;

	//init as DADGAD but in C major (midi note numbers, not freq)
	ps->symp_notes1 = 43;
	ps->symp_notes2 = 48;
	ps->symp_notes3 = 53;
	ps->symp_notes4 = 55;
	ps->symp_notes5= 60;
		
}


//function to change string tuning based on midi control mesasges from TouchOSC 
static void change_string(uint8_t ctrl, uint8_t val, struct patch_state *ps){
	if(val == 0){
	} else {
		//find out which string we are changing
		int idx = ((((int)ctrl - 15) - ((int)ctrl-15)%2)/2);
		switch(idx){
			//and then change it either up or down. max min functions used to bound (bounds based on memory constraints)
			case 0: ps->symp_notes1 = min(127, max(41, ps->symp_notes1 + (1 - 2*(ctrl % 2 ))));
			break;
			case 1: ps->symp_notes2 = min(127, max(41, ps->symp_notes2 + (1 - 2*(ctrl % 2 ))));
			break;
			case 2: ps->symp_notes3 = min(127, max(53, ps->symp_notes3 + (1 - 2*(ctrl % 2 ))));
			break;
			case 3: ps->symp_notes4 = min(127, max(53, ps->symp_notes4 + (1 - 2*(ctrl % 2 ))));
			break;
			case 4: ps->symp_notes5 = min(127, max(53, ps->symp_notes5 + (1 - 2*(ctrl % 2 ))));
			break;
			default: break;
		}

	}

}

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 */
//int to string, needed to explicitly declare as it was not available in gcc libs
static char* itoa(int value, char* result, int base) {
	// check that the base if valid
	if (base < 2 || base > 36) { *result = '\0'; return result; }

	char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;

	do {
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	} while ( value );

	// Apply negative sign
	if (tmp_value < 0) *ptr++ = '-';
	*ptr-- = '\0';
	while(ptr1 < ptr) {
		tmp_char = *ptr;
		*ptr--= *ptr1;
		*ptr1++ = tmp_char;
	}
	return result;
}


//converts a midi note number into a string of the note (incl. sharps and octave)
static void midi2notestr(int note_number, char *note_string){
	
	//formulas for calculating what note and octave
	float octave = ((float)note_number/12) - 1;
	char temp[8];
	int note_idx = (note_number - 24)%12;

	//empty the string
	strcpy(note_string, "");

	//switch case to map the notes to letters then concat with sharp etc.
	switch (note_idx){

	case 0:
	sprintf(note_string, "C");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");
	break;

	case 1:
	sprintf(note_string, "C#");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");	 			
	break;

	case 2:
	sprintf(note_string, "D");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " "); 			
	break;

	case 3:
	sprintf(note_string, "D#");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 4:
	sprintf(note_string, "E");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 5:
	sprintf(note_string, "F");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 6:
	sprintf(note_string, "F#");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 7:
	sprintf(note_string, "G");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 8:
	sprintf(note_string, "G#");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 9:
	sprintf(note_string, "A");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 10:
	sprintf(note_string, "A#");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");		
	break;

	case 11:
	sprintf(note_string, "B");
	strcat(note_string, itoa(octave, temp, 10));
	strcat(note_string, " ");	
	break;

	default:
	break;
	}

}


//midi control functions 
static void control_change(struct patch *p, uint8_t ctrl, uint8_t val) {

	//bring in the patch state and the display driver
	struct patch_state *ps = (struct patch_state *)p->state;
	struct display_drv *ds = (struct display_drv *)p->synth->display;

	//init note string and blank string to concat to
	char note_string[] = "ERROR\n";
	char bank_string[20]="";


	//switch on the midi control value
	switch (ctrl) {
	case 1:		// volume
		ps->vol = midi_map(val, 0.f, 1.5f);
		break;
	//controls for string model parameters
	case 3:		// polarization plane mix
		ps->pol_mix = midi_map(val, 0.f, 1.5f);
		break;
	case 4:		// detune of planes
		ps->detune = midi_map(val, 0.f, 1.f);
		break;
	case 5:		// feedback control for ks loops
		ps->feedback = midi_map(val, 0.96f, 1.0115f);
		break;

	case 6:		// pluck position
		ps->pluck = midi_map(val, 0.01f, 0.5f);
		break;
	case 7:		// coupling mix
		ps->coupling = midi_map(val, 0.f, 0.2f);
		break;

	case 8:		// sympathetic feedback coeff
		ps->symp_feedback = midi_map(val, 0.7f, 1.f);
		break;

	case 9:		// bridge coefficient
		ps->bridge_cutoff = midi_map(val, 1.f, 21000.f);
		break;


	//the following cases all correspond to a string change, so they all invoke 
	// the same commands. logic on the ctl values is used to figure out what to do next							
	case 15:
	case 16:
	case 17:
	case 18:
	case 19:
	case 20:
	case 21:
	case 22:
	case 23:
	case 24:

		//only run the code when the button is depressed, not on release
		if(val==0)
			break;

		//change the string in question by invoking functions above
		change_string(ctrl, val, ps);

		//update the string to be printed
		midi2notestr(ps->symp_notes1, note_string);
		strcat(bank_string, note_string);
		midi2notestr(ps->symp_notes2, note_string);
		strcat(bank_string, note_string);
		midi2notestr(ps->symp_notes3, note_string);
		strcat(bank_string, note_string);
		midi2notestr(ps->symp_notes4, note_string);
		strcat(bank_string, note_string);
		midi2notestr(ps->symp_notes5, note_string);
		strcat(bank_string, note_string);
		strcat(bank_string, "\n");

		//print out to the lcd the new string
		print_one_line(&ds->term, bank_string);
		break;

	default:
		break;
	}

}

//patch functions struct
const struct patch_ops patch = {
	.start = start,
	.note_on = note_on,
	.generate = generate,
	.init = init,
	.control_change = control_change,
};
